import * as Realm from "realm-web"

const app = new Realm.App({id: "application-marketplace-saxov"})
export { app }