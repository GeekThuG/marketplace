import * as Realm from "realm-web";
import { app } from "../../service/mongoDB-sdk";
import { handleAuthenticationErrors, handleLogin } from "../../state/actions/authentication";
import { addUser, getUser } from "../../service";


const useAuthentication = (dispatch) => {   
  function handleUserRegistation(newUser) {
    const userProfile = {
      ...newUser,
      password: undefined,
      confirm_password: undefined
    }
    return new Promise((resolve) => {
      app.emailPasswordAuth
        .registerUser(newUser.email, newUser.password)
        .then(() => {
          const credentials = Realm.Credentials.emailPassword(
            newUser.email,
            newUser.password
          );
          app.logIn(credentials).then((user) => {
            addUser(userProfile)
            dispatch(handleLogin(userProfile))
            resolve(user)
          });
        }).catch(err => dispatch(handleAuthenticationErrors))
    })
  }

  async function handleUserLogout() {
    console.dir(app.currentUser);
    app.currentUser
      ?.logOut()
      .then(() =>  console.log("user successfully log out"))
      .catch((err) => console.log(err));
  }

  async function handleUserLogin(email, password) {
    return new Promise((resolve) => {
      app
        .logIn(Realm.Credentials.emailPassword(email, password))
        .then(async () => {
          // verify current user
          const currentUser = await app.currentUser;
          // retrieve user profile
          getUser(currentUser).then((userProfile) => {
            dispatch(handleLogin(userProfile));
            resolve(currentUser);
          });
        })
        .catch((err) => dispatch(handleAuthenticationErrors(err)));
    });
  }

  async function handleAuthentication() {
    const currentUser = await app.currentUser;
    dispatch(handleLogin(currentUser));
    getUser(currentUser?.email)
        .then(userProfile => !!currentUser && dispatch(handleLogin(userProfile)))
        .catch(err =>  dispatch(handleAuthenticationErrors(err)))
  }


  return {
    handleUserRegistation,
    handleUserLogout,
    handleUserLogin,
    handleAuthentication,
  }
};

export default useAuthentication